export default {
    state: {
        category: [],
        brand: [],
        product: []
    },
    getters: {
        category_parent(state) {
            return state.category.filter((q) => q.c_subid == 0);
        },
        categoryByID: (state) => (id) => {
            return state.category.find((q) => q.id == id);
        },
        subCategoryByID: (state) => (id) => {
            return state.category.filter((q) => q.c_subid === id);
        },
        Cmenu(state) {
            let store = state.category;
            let cMenu = [{
                c_name_TH: 'สินค้า',
                id: 0
            }].concat(store);
            return cMenu;
        },
        ProductByCID: (state) => (id) => {
            let findsub = (category, matchID) => {
                let find = category.filter((q) => q.c_subid == matchID);
                Allarray.push(parseInt(matchID));
                if (find.length > 0) {
                    for (let i in find) {
                        findsub(category, find[i].id);
                    }
                }
            }
            let Allarray = [];
            const Category = state.category;
            const Product = state.product;
            let CallFunction = findsub(Category, id);
            let product = [];
            for (let index in Allarray) {
                let result = Product.filter((q) => q.c_id == Allarray[index]);
                for (let x in result) {
                    product.push(result[x]);
                }
            }
            return _.orderBy(product,'c_id');
        },
        ProductById: (state) => (id) => {
            return state.product.find((q) => q.id == id);
        },
        checkProductCode: (state) => (code) => {
            let result = _.find(state.product, {'p_code' : code});
            return result;
        }
    },
    mutations: {
        setCategory(state, data) {
            state.category = data;
        },
        setBrand(state, data) {
            state.brand = data;
        },
        setProduct(state, data) {
            state.product = data;
        }
    },
    actions: {
        async getCategory({
            commit
        }) {
            let response = await axios.get('/category').then((res) => {
                commit('setCategory', res.data);
            })
        },
        async getBrand({
            commit
        }) {
            let response = await axios.get('/brand').then((res) => {
                commit('setBrand', res.data);
            })
        },
        async getProduct({
            commit
        }) {
            let response = await axios.get('/product').then((res) => {
                commit('setProduct', res.data);
            })
        }
    }
}